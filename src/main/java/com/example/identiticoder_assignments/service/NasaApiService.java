package com.example.identiticoder_assignments.service;

import com.example.identiticoder_assignments.config.NasaApiConfig;
import com.example.identiticoder_assignments.model.Asteroid;
import com.example.identiticoder_assignments.model.Neo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NasaApiService {

    @Autowired
    private NasaApiConfig nasaApiConfig;

    @Autowired
    private RestTemplate restTemplate;

    public List<Asteroid> getTop10ClosestAsteroids(String startDate, String endDate) {
        String url = String.format("%s/neo/rest/v1/feed?start_date=%s&end_date=%s&api_key=%s",
                nasaApiConfig.getBaseUrl(), startDate, endDate, nasaApiConfig.getApiKey());

        Neo response = restTemplate.getForObject(url, Neo.class);

        return response.getNearEarthObjects().values().stream()
                .flatMap(Collection::stream)
                .sorted(Comparator.comparingDouble(Asteroid::getMissDistance))
                .limit(10)
                .collect(Collectors.toList());
    }
}
