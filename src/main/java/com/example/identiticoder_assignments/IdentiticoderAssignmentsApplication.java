package com.example.identiticoder_assignments;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IdentiticoderAssignmentsApplication {

    private static final Logger log = LoggerFactory.getLogger(IdentiticoderAssignmentsApplication.class);

    public static void main(String[] args) {
        log.info("In main method");
        SpringApplication.run(IdentiticoderAssignmentsApplication.class, args);
    }

}
