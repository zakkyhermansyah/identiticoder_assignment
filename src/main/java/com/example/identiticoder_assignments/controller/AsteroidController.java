package com.example.identiticoder_assignments.controller;

import com.example.identiticoder_assignments.model.Asteroid;
import com.example.identiticoder_assignments.service.NasaApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/asteroids")
public class AsteroidController {

    @Autowired
    private NasaApiService nasaApiService;

    @GetMapping("/top10")
    public List<Asteroid> getTop10ClosestAsteroids(@RequestParam String startDate, @RequestParam String endDate) {
        return nasaApiService.getTop10ClosestAsteroids(startDate, endDate);
    }
}
