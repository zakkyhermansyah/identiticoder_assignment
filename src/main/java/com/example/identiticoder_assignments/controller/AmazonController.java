package com.example.identiticoder_assignments.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AmazonController {

    @GetMapping("/aws")
    public ResponseEntity<String> health(){
        return ResponseEntity.ok("Zakky Fadhilah Hermansyah, Identiticoder Assignments");
    }
}
