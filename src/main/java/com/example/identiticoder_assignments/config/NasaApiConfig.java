package com.example.identiticoder_assignments.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NasaApiConfig {
    @Value("${nasa.api.key}")
    private String apiKey;

    @Value("${nasa.api.base.url}")
    private String baseUrl;

    public String getApiKey() {
        return apiKey;
    }

    public String getBaseUrl() {
        return baseUrl;
    }
}
