package com.example.identiticoder_assignments.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Asteroid {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("close_approach_data")
    private List<CloseApproachData> closeApproachData;

    public double getMissDistance() {
        return closeApproachData.get(0).getMissDistance().getKilometers();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
