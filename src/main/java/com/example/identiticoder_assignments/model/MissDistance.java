package com.example.identiticoder_assignments.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MissDistance {
    @JsonProperty("kilometers")
    private double kilometers;

    public double getKilometers() {
        return kilometers;
    }

    public void setKilometers(double kilometers) {
        this.kilometers = kilometers;
    }
}
