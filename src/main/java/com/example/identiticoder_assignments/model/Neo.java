package com.example.identiticoder_assignments.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

public class Neo {

        @JsonProperty("near_earth_objects")
        private Map<String, List<Asteroid>> nearEarthObjects;

        public Map<String, List<Asteroid>> getNearEarthObjects() {
            return nearEarthObjects;
        }

        public void setNearEarthObjects(Map<String, List<Asteroid>> nearEarthObjects) {
            this.nearEarthObjects = nearEarthObjects;
        }

}
