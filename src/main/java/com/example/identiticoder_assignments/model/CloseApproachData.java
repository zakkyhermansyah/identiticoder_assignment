package com.example.identiticoder_assignments.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CloseApproachData {

    @JsonProperty("miss_distance")
    private MissDistance missDistance;

    public MissDistance getMissDistance() {
        return missDistance;
    }

    public void setMissDistance(MissDistance missDistance) {
        this.missDistance = missDistance;
    }
}
